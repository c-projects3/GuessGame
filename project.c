#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>

#define GETPIN rand() % (6 - 1 + 1) + 1 //use to geterate the pin
#define CRAZY rand() % (25 - 1 + 1) + 1 // use the geterate the random/crazy choice
#define CHARTOINT '0' // use to convert the char to int

enum level{Easy = 20, Moderate = 15, Hard = 10} gameLevel; //game level, note: the CRAZY(define) could be the game level

int left = 0; // to print how many round its took
int choice = 0; // use to get the choice (also, to check if the choice was 4 --> dont show how many rounds left)
int flag = 1; //use to print the lose msg (because if the user won, he shouldnt see the lose msg)
int pin1 = 0, pin2 = 0, pin3 = 0, pin4 = 0; //globals varibals, use to generate the PIN
char gues1 = ' ', gues2 = ' ', gues3 = ' ', gues4 = ' '; //globals varibals, use to get the gues from the client (char - to find Invalid input easly)
int num1 = 0, num2 = 0, num3 = 0, num4 = 0; //globals varibals, use to convert the char gues to int gues 


void startMsg(void); // the start msg (rules and deraction)
void getLevel(void); // get the game level from the user
void getPin(void); // generate a new PIN
void getGues(void); // get the gues from the user (char)
int guesLegal(void); // convert the char to int and check if the gues is legal (any number in the range (1 -6)) 
int hitsAndMiss(int round); // count how many HITS & MISS was in the current gues (Update the rounde left, and check if the user won)
void loseMsg(void); // show a lose msg


int main()
{
    int roundLeft = 0;
    char again = ' '; 
    srand(time(NULL)); // generate a new seed
    startMsg();
    getLevel();

    roundLeft = gameLevel;
    left = gameLevel;
    getPin();

    do{
        do
        {
            getGues(); // get gues and check if it legal
        } while (guesLegal());

        roundLeft = hitsAndMiss(roundLeft); // check how many hits and miss 
    }while(roundLeft); // end of the round

    if(flag){ // check if the round end (and the user dosnt won)
        loseMsg(); //print the lose msg
    }
    
    do{
        printf("\nPlay again? (y/n)\n"); // ask the user if he want to play again
        again = getch();
        printf("%c", again); // print the usr choice (getch dont show anything to the screen)
    }while((again != 'n') && (again != 'y')); // check if the input is legal

    if(again == 'y'){ //check if the user want to play again
        main(); // call the main to play again
    }
    printf("\nBye Bye\n\n");
    return 0;
}





/*  The func print the ruls 
    input: none
    output: none
*/
void startMsg(void)

{
    printf("Welcome to \"MAGSHIMIM CODE-BREAKER\"!!!\n\n");

    printf("A secret password was chosen to protect the credit card of Pancratius,\n");
    printf("the descendant of Antiochus.\n");
    printf("Your mission is to stop Pancratius by revealing his secret password.\n\n");

    printf("The rules are as follows:\n");
    printf("1. In each round you try to guess the secret password (4 distinct digits)\n");
    printf("2. After every guess you'll receive two hints about the password\n");
    printf("HITS:   The number of digits in your guess which were exactly right.\n");
    printf("MISSES: The number of digits in your guess which belongs to\n");
    printf("        the password but were miss-placed.\n");
    printf("3. If you'll fail to guess the password after a certain number of rounds\n");
    printf("Pancratius will buy all the gifts for Hanukkah!!!\n\n");

    printf("Please choose the game level:\n");
    printf("1 - Easy (20 rounds)\n");
    printf("2 - Moderate (15 rounds)\n");
    printf("3 - Hard (10 rounds)\n");
    printf("4 - Crazy (random number of rounds 5-25)\n\n");

}



/* The func will get input, and check if he legal (1 - 4), after that the gameLevel (in enum) will change according to the user choice
input: none
output: none
*/
void getLevel(void)
{
    int try = 0; // try - to check if the user sould see try again ,choice - to get the choice after to convertion 
    char chlevel = ' '; // using char and getch to get the number (I convert in into a int)
    
    do{
        if(++try > 1){ //check if should i print an error msg
            printf("Invalid input, Try again\n");
        }
        
        chlevel = getch(); // get the choice
        printf("%c\n", chlevel); // print the usr choice (getch dont show anything to the screen)
        choice = (int)chlevel - (int)CHARTOINT; // convert the choice to int 

    }while((choice > 4) || (choice < 1)); //check if the number is legal level

    switch (choice) // change the game level according to user choice
    {
    case 1:
        gameLevel = Easy;
        break;
    case 2:
        gameLevel = Moderate;
        break;
    case 3:
        gameLevel = Hard;
        break;
    case 4:
        gameLevel = CRAZY;
        break;
    
    default:
        break;
    }
}


/*  The func will take the global varible (pin1, pin2 ...) and choose a random option for them, but the num sould be different
    input:none
    output: none
*/
void getPin(void)
{
    do{
        pin1 = GETPIN; 
        pin2 = GETPIN;
        pin3 = GETPIN;
        pin4 = GETPIN;
    }while((pin1 == pin2) || (pin1 == pin3) || (pin1 == pin4) || (pin2 == pin3) || (pin2 == pin4) || (pin3 == pin4)); //check if some number apper more then one
    
}


/*  the func get the gues from the user (char), and presante the choice (because the getch show nothing to the screen)
    input: none
    output: none
*/
void getGues(void)
{
    printf("\nWrite your guess (only 1-6, no ENTER is needed)\n"); // ask for gues
    gues1 = getch(); //get the gues
    putch(gues1); // print the usr choice (getch dont show anything to the screen)
    gues2 = getch();
    putch(gues2);
    gues3 = getch();
    putch(gues3);
    gues4 = getch();
    printf("%c\n\n", gues4);
}


/*  The func take the gues (global varible' char) convert the to int, and check if the in the range(1 -6)
    input: none
    output: 0 --> if its work (to end  the INPUT_CODE loop), 1--> to continue the loop*/
int guesLegal(void){
    int legal = 0;
    num1 = (int)gues1 - (int)CHARTOINT; // convert the numbers to char 
    num2 = (int)gues2 - (int)CHARTOINT; // NOTE TO RAN: i think about convert func (but i decide not)
    num3 = (int)gues3 - (int)CHARTOINT;
    num4 = (int)gues4 - (int)CHARTOINT;
    legal = ((num1 > 6) || (num1 < 1)) ? 0 : ++legal; // check if the numers in the raange
    legal = ((num2 > 6) || (num2 < 1)) ? 0 : ++legal;
    legal = ((num3 > 6) || (num3 < 1)) ? 0 : ++legal;
    legal = ((num4 > 6) || (num4 < 1)) ? 0 : ++legal;
    if(legal == 4){ // check if all the numbers is ok
        return 0; // to end the loop (loop - check if the input is ok)
    }
    return 1; // the loop should continue until the legal gues
}


/*  The func check how many hits and how many miss was in the current guse, check if the gues is the PIN and if not, print the HITS & NISSES
    input: current round (to print him to the screen)
    output: the new round (round--), the usr gues right --> round = 0 (to end the loop*/
int hitsAndMiss(int round)
{
    int hits = 0, miss = 0;
    round--; // down the "round left"

    hits = (num1 == pin1) ? ++hits : hits; //check if something is hit
    hits = (num2 == pin2) ? ++hits : hits;
    hits = (num3 == pin3) ? ++hits : hits;
    hits = (num4 == pin4) ? ++hits : hits; 
    
    miss = ((num1 == pin2) || (num1 == pin3) || (num1 == pin4)) ? ++miss : miss; //check if something is miss
    miss = ((num2 == pin1) || (num2 == pin3) || (num2 == pin4)) ? ++miss : miss;
    miss = ((num3 == pin1) || (num3 == pin2) || (num3 == pin4)) ? ++miss : miss;
    miss = ((num4 == pin1) || (num4 == pin2) || (num4 == pin3)) ? ++miss : miss;
    
    if(hits == 4) { // check if the user gues correctly
        printf("    4 HITS YOU WON!!!\n");
        printf("It took you only %d guesses, you are a professional code breaker!", left - round);
        round = 0; // to end the loop
        flag = 0; // to dont show lose msg
    }
    else{ // update the usr how many HITS & MISS he gues
        printf("You got    %d HITS    %d MISSES.\n", hits, miss);
        if(choice != 4){ // check if the level was crazy (dont show how many round left)
            printf("%d guesses left\n", round);
        }
        
    }
    return round; // return the curent round
    
}


/*  The func print lose message 
    inpur: none
    output: none*/
void loseMsg(void){
    printf("OOOOHHHH!!! Pancratius won and bought all of Hanukkah's gifts.\n");
    printf("Nothing left for you...\n");
    printf("The secret password was %d%d%d%d\n", pin1, pin2, pin3, pin4);
}
